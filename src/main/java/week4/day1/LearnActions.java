package week4.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class LearnActions {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "C:\\Workspace\\Selenium_java\\drivers\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		//driver.get("https://www.irctc.co.in/eticketing/loginHome.jsf/");
		driver.get("https://jqueryui.com/draggable/");
		//driver.manage().window().maximize();
		//Thread.sleep(3000);
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		driver.switchTo().frame(0);
		WebElement drag = driver.findElementByXPath("//div[@id='draggable']/p");
		//WebElement drop = driver.findElementByXPath("//div[@id='droppable']/p");
		Actions builder = new Actions(driver);
		builder.dragAndDropBy(drag, 100, 150).perform();

	}

}
