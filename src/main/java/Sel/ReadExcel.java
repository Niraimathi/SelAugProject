package Sel;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {
	public static Object[][] getDataExcel(String fileName) throws IOException {
		XSSFWorkbook wbook = new XSSFWorkbook("./data/"+fileName+".xlsx");
		XSSFSheet sheet = wbook.getSheetAt(0);
		// basic code of hard coding row number and cell number
		/* XSSFRow row = sheet.getRow(1);
		XSSFCell cell = row.getCell(1);
		String stringCellValue = cell.getStringCellValue();
		System.out.println(stringCellValue); */
		int lastRowNum = sheet.getLastRowNum();
		System.out.println(lastRowNum);
		short lastCellNum = sheet.getRow(0).getLastCellNum();
		Object[][] data = new Object[lastRowNum][lastCellNum];
		System.out.println(lastCellNum);
		for (int i=1;i<=lastRowNum;i++)
		{
			XSSFRow row = sheet.getRow(i);
			for (int j=0;j<lastCellNum;j++)
			{
				XSSFCell cell = row.getCell(j);
				String stringCellValue = cell.getStringCellValue();
				data[i-1][j] = stringCellValue;
				System.out.println(stringCellValue); 
			}
		}
		
		wbook.close();
		return data;
		}
		
	}


