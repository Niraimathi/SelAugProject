package week3.day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class testleaf {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "C:\\Workspace\\Selenium_java\\drivers\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/");
		// To maximize the screen
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		
		// To provide the input for the create form
		driver.findElementById("createLeadForm_companyName").sendKeys("TEST LEAF");
		driver.findElementById("createLeadForm_firstName").sendKeys("Muhil");
		//driver.findElementById("createLeadForm_parentPartyId").sendKeys("1230");
		
		driver.findElementById("createLeadForm_lastName").sendKeys("Sai");
		WebElement src = driver.findElementById("createLeadForm_dataSourceId");
		Select obj = new Select(src);
		obj.selectByIndex(7);
		WebElement src1 = driver.findElementById("createLeadForm_marketingCampaignId");
		Select obj1 = new Select(src1);
		obj1.selectByValue("CATRQ_CAMPAIGNS");
		WebElement src2 = driver.findElementById("createLeadForm_industryEnumId");
		Select obj2 = new Select(src2);
		obj2.selectByValue("IND_HARDWARE");
		WebElement src3 = driver.findElementById("createLeadForm_ownershipEnumId");
		Select obj3 = new Select(src3);
		obj3.selectByVisibleText("Partnership");
		
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("XYZ");
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("Pvt. LTD");
		
		driver.findElementById("createLeadForm_personalTitle").sendKeys("Dear");
		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Mr.");
		driver.findElementById("createLeadForm_departmentName").sendKeys("Training");
		driver.findElementById("createLeadForm_annualRevenue").sendKeys("40000000");
		WebElement sss = driver.findElementById("createLeadForm_currencyUomId");
		Select obj0= new Select(sss);
		obj0.selectByVisibleText("DRP - Dominican Republic Peso");
		driver.findElementById("createLeadForm_numberEmployees").sendKeys("40");
		driver.findElementById("createLeadForm_sicCode").sendKeys("1083");
		driver.findElementById("createLeadForm_tickerSymbol").sendKeys("yes");
		driver.findElementById("createLeadForm_description").sendKeys("Training center for IT Trainings");
		driver.findElementById("createLeadForm_importantNote").sendKeys("To educate IT Trainings");
		
		//driver.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("9");
		
		driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("44");
		driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("434");
		driver.findElementById("createLeadForm_primaryEmail").sendKeys("xyz@testleaf.com");
		driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("9089090009");
		driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("xyz");
		driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("http://www.testleaf.com");
		
		
		driver.findElementById("createLeadForm_generalToName").sendKeys("dshkjfds");
		driver.findElementById("createLeadForm_generalAttnName").sendKeys("lkdjfjaakuhakfd");
		driver.findElementById("createLeadForm_generalAddress1").sendKeys("9 Amman koil Street, palavanthaangal");
		driver.findElementById("createLeadForm_generalAddress2").sendKeys("near railway junction");
		WebElement src4 = driver.findElementById("createLeadForm_generalCountryGeoId");
		Select obj4 = new Select(src4);
		obj4.selectByVisibleText("India");
//		WebElement src5 = driver.findElementById("createLeadForm_generalStateProvinceGeoId");
//		Select obj5 = new Select(src5);
//		obj5.selectByVisibleText("TamilNadu");
		
		driver.findElementById("createLeadForm_generalCity").sendKeys("Chennai");
		driver.findElementById("createLeadForm_generalPostalCode").sendKeys("600081");
		driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("+91");
		
		
		driver.findElementByName("submitButton").click();
	}

}
