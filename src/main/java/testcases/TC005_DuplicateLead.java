package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;

import org.testng.annotations.Test;





public class TC005_DuplicateLead extends ProjectMethod{

	

	@BeforeTest(groups = "common")

	public void setData() {

		TestCaseName = "TC005";

		TestCaseDescription ="Duplicate a lead";

		category = "Regression";

		author= "xyz";

	}

	@Test(groups="regression",priority = 4)

	public void createLead() throws InterruptedException {

		WebElement ele4 = locateElement("link", "CRM/SFA");
		click(ele4);
		click(locateElement("link", "Leads"));

		click(locateElement("link", "Find Leads"));

		click(locateElement("xpath", "//span[contains(text(),'Phone')]"));

		type(locateElement("name", "phoneNumber"), "9");

		click(locateElement("xpath", "//button[contains(text(),'Find Leads')]"));

		Thread.sleep(2000);

		String txt = getText(locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a"));

		System.out.println(txt);

		clickWithNoSnap(locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a"));

		click(locateElement("link", "Duplicate Lead"));

		click(locateElement("xpath", "//input[@name='submitButton']"));				

	}

}


