package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class TC002_CreateLead extends ProjectMethod{
	@BeforeClass//(groups="common")
	public void startTestcase()
	{
		TestCaseName = "TC002";
		TestCaseDescription = "create Lead";
		author = "Test1";
		category = "sanity";
		excelFileName="Create Lead";
	}
	//@Test (groups= "Smoke",dataProvider="CreateLead",priority=1)//,invocationCount=2,invocationTimeOut = 3000)
	@Test(dataProvider="fetchData")
	public void CreateLead(String cName,String fName,String lName,String ph,String email) {
//		startApp("chrome", "http://leaftaps.com/opentaps");
//		WebElement eleUserName = locateElement("id", "username");
//		type(eleUserName, "DemoSalesManager");
//		WebElement elePassword = locateElement("id","password");
//		type(elePassword, "crmsfa");
//		WebElement eleLogin = locateElement("class","decorativeSubmit");
//		click(eleLogin);
		
		WebElement elecrm = locateElement("link", "CRM/SFA");
		click(elecrm);
		WebElement elecrlead = locateElement("link", "Create Lead");
		click(elecrlead);
		WebElement elecomname = locateElement("id", "createLeadForm_companyName");
		type(elecomname, cName);
		WebElement elefirst = locateElement("id","createLeadForm_firstName");
		type(elefirst, fName);
		WebElement elelast = locateElement("id","createLeadForm_lastName");
		type(elelast, lName);
		WebElement elephone = locateElement("id","createLeadForm_primaryPhoneNumber");
		type(elephone, ph);
		WebElement eleemail = locateElement("createLeadForm_primaryEmail");
		type(eleemail, email);
		/*WebElement elesource = locateElement("id","createLeadForm_dataSourceId");
		selectDropDownUsingText(elesource, "Conference");
		WebElement elemkt = locateElement("id","createLeadForm_marketingCampaignId");
		selectDropDownUsingText(elemkt, "Automobile");
		WebElement eleind = locateElement("id", "createLeadForm_industryEnumId");
		selectDropDownUsingIndex(eleind, 10);*/
		WebElement elecrsub = locateElement("name", "submitButton");
		click(elecrsub);
		/*WebElement elefnlead = locateElement("id", "viewLead_firstName_sp");
//		String text = elefnlead.getText();
		verifyExactText(elefnlead, "Test");
		WebElement elefir = locateElement("name","firstName");
		type(elefir,"Muhil");
		*/
				
	
	}
	

}