package testcases;

import Sel.ReadExcel;
import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import Sel.ReadExcel;
import wdMethods.SeMethods;

public class ProjectMethod extends SeMethods {
	@DataProvider(name="fetchData")
	public Object[][] getData() throws IOException
	{
		Object[][] dataExcel = ReadExcel.getDataExcel(excelFileName);
		return dataExcel;
		
	}
	@BeforeMethod //(groups="common")
	@Parameters({"browser","appUrl","userName","password"})
	public void login(String browser,String url,String uName,String pwd)
	
	{
		startApp(browser, url);
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, uName);
		WebElement elePassword = locateElement("id","password");
		type(elePassword, pwd);
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		takeSnap();
	}
//	@AfterMethod
//	public void closeAllBrowsers()
//	{
//		driver.quit();
//	}
	
	

}
