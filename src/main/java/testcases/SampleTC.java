package testcases;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;

import org.testng.annotations.Test;




public class SampleTC extends ProjectMethod{



	@BeforeTest(groups = "common")

	public void setData() {

		TestCaseName = "TC004";

		TestCaseDescription ="Delete a lead";

		category = "Sanity";

		author= "Sethu";

	}



	@Test(groups = "regression", dependsOnGroups = "sanity"

			/*,priority =1,

			dependsOnMethods = 

		{"testng.CreateLead.createLead",

				"testng.CreateLead.editlead"}, 

			alwaysRun = true,

			enabled = false*/)

	public void deleteLead() throws InterruptedException {

		WebElement ele4 = locateElement("link", "CRM/SFA");
		click(ele4);

		click(locateElement("link", "Leads"));

		click(locateElement("link", "Find Leads"));

		type(locateElement("xpath", "(//input[@name='firstName'])[3]"), "K");

		click(locateElement("xpath", "//button[contains(text(),'Find Leads')]"));

		Thread.sleep(3000);

		String txt = getText(locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a"));

		click(locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a"));

		verifyTitle("View Lead | opentaps CRM");

		click(locateElement("link", "Delete"));		

		click(locateElement("link", "Find Leads"));

		type(locateElement("xpath", "//label[contains(text(),'Lead ID:')]/following::input"), txt);

		click(locateElement("xpath", "//button[contains(text(),'Find Leads')]"));

		verifyPartialText(locateElement("xpath", "//div[@class='x-paging-info']"), "No records to display");		

	}

}