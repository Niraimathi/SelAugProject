package testcases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class TC003_EditLead extends ProjectMethod{
	@BeforeClass(groups= "common")
	public void startTestcase()
	{
		TestCaseName = "TC003";
		TestCaseDescription = "Edit Lead";
		author = "Test";
		category = "sanity";
	}
	@Test(groups="sanity",priority=2)
	public void editLead() {
//		startApp("chrome", "http://leaftaps.com/opentaps");
//		WebElement eleUserName = locateElement("id", "username");
//		type(eleUserName, "DemoSalesManager");
//		WebElement elePassword = locateElement("id","password");
//		type(elePassword, "crmsfa");
//		WebElement eleLogin = locateElement("class","decorativeSubmit");
//		click(eleLogin);
		
		
		
//		WebElement elecrm = locateElement("link", "CRM/SFA");
//		click(elecrm);
//		WebElement eleleads = locateElement("link","Leads");
//		click(eleleads);
//		WebElement elefindleads = locateElement("link","Find Leads");
//		click(elefindleads);
//		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
//		WebElement elefir = locateElement("xpath","(//input[@name='firstName'])[3]");
//		type(elefir,"Test");
//		
//		WebElement elefindleads1 = locateElement("xpath","(//button[text()='Find Leads'])");
//		click(elefindleads1);
		
		WebElement ele4 = locateElement("link", "CRM/SFA");
		click(ele4);
		WebElement ele5 = locateElement("link", "Leads");
		click(ele5);
		WebElement ele6 = locateElement("link", "Find Leads");
		click(ele6);
		WebElement ele7 = locateElement("xpath", "(//input[@name='firstName'])[3]");
		type(ele7,"Test");
		WebElement ele8 = locateElement("xpath", "//button[text()='Find Leads']");
		click(ele8);
		WebElement ele9 = locateElement("xpath", "//table[contains(@class,'row')]//a");
		click(ele9);
		verifyTitle("View Lead | opentaps CRM");
		WebElement ele10 = locateElement("link","Edit");
		click(ele10);
		WebElement ele11 = locateElement("id","updateLeadForm_companyName");
		ele11.clear();
		type(ele11,"Adp India Pvt Ltd,chennai");
		WebElement ele12 = locateElement("xpath","//input[@name='submitButton'][1]");
		click(ele12);
		//closeBrowser();

		
	}
}
