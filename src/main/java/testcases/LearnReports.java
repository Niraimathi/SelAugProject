package testcases;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnReports {
	
	

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		ExtentHtmlReporter html = new ExtentHtmlReporter("./Reports/result.html");
		html.setAppendExisting(true);
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(html);
		ExtentTest test = extent.createTest("TC001_CreateLead","Create a New Lead");
		test.pass("The data DemoSalesManager Entered successfully",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		test.assignAuthor("test");
		test.assignCategory("Sanity");
		test.pass("The password crmsfa entered successfully");
		test.pass("login successfully");
		extent.flush();
				

	}

}
