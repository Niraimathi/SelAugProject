package testcases;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;

import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnReports1 {
	public static ExtentTest test;
	public static ExtentReports extent;
	public static String TestCaseName,TestCaseDescription,author,category,excelFileName;
	
	@BeforeSuite//(groups="common")
	public void startResult()
	{
		ExtentHtmlReporter html = new ExtentHtmlReporter("./Reports/result.html");
		html.setAppendExisting(true);
		extent = new ExtentReports();
		extent.attachReporter(html);
	}
	
	@BeforeMethod//(groups="common")
	public void startTestCase()
	{
		 test = extent.createTest(TestCaseName,TestCaseDescription);
		//test.pass("The data DemoSalesManager Entered successfully",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		test.assignAuthor(author);
		test.assignCategory(category);
		
	}
	
	public void reportStep(String desc,String status)
	{
		if (status.equalsIgnoreCase("pass"))
		{
			test.pass(desc);
		} if (status.equalsIgnoreCase("fail"))
		{
			test.fail(desc);
		}
	}
	
	@AfterSuite//(groups="common")
	public void stopResult()
	{
		extent.flush();
	}

}
