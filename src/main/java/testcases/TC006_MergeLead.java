package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;

import org.testng.annotations.Test;





public class TC006_MergeLead extends ProjectMethod{

	

	@BeforeTest(groups="common")

	public void setData() {

		TestCaseName = "TC006";

		TestCaseDescription ="Merge 2 leads";

		category = "Regression";

		author= "xyz";

	}



	@Test(groups="regression",priority=5)

	public void createLead() throws InterruptedException {
		WebElement ele4 = locateElement("link", "CRM/SFA");
		click(ele4);

		click(locateElement("link", "Leads"));

		click(locateElement("link", "Merge Leads"));

		click(locateElement("xpath", "(//img[@alt='Lookup'])[1]"));

		switchToWindow(1);

		type(locateElement("name", "firstName"), "test");

		click(locateElement("xpath", "//button[contains(text(),'Find Leads')]"));

		Thread.sleep(2000);

		String txt = getText(locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a"));

		clickWithNoSnap(locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a"));

		switchToWindow(0);

		

		click(locateElement("xpath", "(//img[@alt='Lookup'])[2]"));

		switchToWindow(1);

		type(locateElement("name", "firstName"), "K");

		click(locateElement("xpath", "//button[contains(text(),'Find Leads')]"));

		Thread.sleep(2000);		

		clickWithNoSnap(locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a"));

		switchToWindow(0);

		

		clickWithNoSnap(locateElement("link", "Merge"));

		acceptAlert();

		

		click(locateElement("link", "Find Leads"));

		type(locateElement("xpath", "//label[contains(text(),'Lead ID:')]/following::input"), txt);

		click(locateElement("xpath", "//button[contains(text(),'Find Leads')]"));

		verifyPartialText(locateElement("xpath", "//div[@class='x-paging-info']"), "No records to display");		

	}

}


