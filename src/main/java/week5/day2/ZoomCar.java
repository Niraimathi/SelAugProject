package week5.day2;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ZoomCar {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "C:\\Workspace\\Selenium_java\\drivers\\chromedriver.exe");
		//System.setProperty("webdriver.chrome.driver", value)
		ChromeDriver driver = new ChromeDriver();

		driver.get("https://www.zoomcar.com/chennai/");
		driver.manage().window().maximize();
		driver.findElementByLinkText("Start your wonderful journey").click();
		Thread.sleep(2000);
		driver.findElementByXPath("//div[contains(text(),'Thuraipakkam')]").click();
		driver.findElementByXPath("//button[contains(text(),'Next')]").click();
		Thread.sleep(2000);
		driver.findElementByXPath("//div[contains(text(),'Mon')]").click();
		driver.findElementByXPath("//button[contains(text(),'Next')]").click();	
		//driver.findElementByXPath("//button[@class=\"proceed\"]").click();
		WebElement val = driver.findElementByXPath("//div[@class='day picked ']/div");
		String text = val.getText();
		if (text.equals("Mon"))
		{
			System.out.println("both matching");
		}
		else
		{System.out.println("Not matching");}
	}

}
